// ******** click event on search ***************
$(".search_text").click(function(e) {
  e.preventDefault();
  $(".search_input").css("display", "block");
});


// ******  after pressing enter key, the search input hides again *****
$(".search_input").on("enterKey", function() {
  $(this).css("display", "none");
});


// *************** LOAD AND SCROLL EVENTS ***************************
$(window).on('load resize scroll', function() {
    //height of intro section
    var intro_height = $("#intro-section").height();

    //save current scroll value
    var current_scroll = $(this).scrollTop();

    //scroll value of quotes-section
    var quote_scroll = $("#quote-section").offset().top;

    //distance of #posts relative to the top
    var posts_scroll = $("#posts").offset().top;

    //distance of #why section relative to the top
    var why_scroll = $("#why-section").offset().top;

    //why section heigth
    var why_height = $("#why-section").height();

    //scroll events for why permaculture texts
    if(current_scroll > intro_height/2) {
      $(".article-text:nth-child(2)").fadeTo('slow', 1);
    }
    if(current_scroll > why_scroll) {
      //$(".article-text:nth-child(2)").fadeTo('slow', 0);
      $(".article-text:nth-child(3)").fadeTo('slow', 1);
    }
    if(current_scroll > why_scroll + why_scroll/3) {
      //$(".article-text:nth-child(2)").fadeTo('slow', 0);
      $(".article-text:nth-child(4)").fadeTo('slow', 1);
    }


    //change menu icon color
    if(current_scroll > intro_height) {
      $(".menu").css("color", "black");
    } else {
      $(".menu").css("color", "white");
    }

    //show quote
    if(current_scroll > quote_scroll-why_height/2) {
      $(".quote").fadeTo('slow', 1);
    }

    // ISOTOPE
    $('#posts').isotope({
      itemSelector: '.post',
      layoutMode: 'masonry'
    });

    $('.section-categorie p').click(function(){
      var cat = $(this).attr('data-cat');
      $('#posts').isotope({filter:'.'+cat});
    });

    $('#search').isotope({
      itemSelector: '.post',
      layoutMode: 'masonry'
    });


    // show back to top button
    if(current_scroll > posts_scroll) {
        $(".top").fadeIn(200);
    } else {
      $(".top").fadeOut(200);
    }
});


// ******************  click events on buttons ***************
//add active class to first button
$(".button").eq(0).addClass("active");
//show and hide of buttons
$(".button").click(function() {
  //save the index of active button
  var current_button = $(".active").index();
  //save index of clicked button
  var clicked_button = $(this).index();
  //remove active class from active button
  $(".active").removeClass("active");
  //adicionar active class to clicked button
  $(this).addClass("active");
  //show topic with same index as the clicked button
  $(".article-text").eq(clicked_button).animate({"opacity": "1"});
  //hide topic with same index as the current button
  $(".article-text").eq(current_button).css({"opacity": "0"});
});


// ****************** click event menu icon **********************
$(".menu i").click(function() {
  $(".dropdown").slideToggle();
});



// *************** click event back to top button ***********************
$(".top").click(function() {
  $("html, body").animate({scrollTop:0}, 900);
});
