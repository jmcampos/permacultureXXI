<?php

    include('includes/init.php');

    //create new object from Post
    $newPost = new Post($db);
    //get all records from posts table
    $posts = $newPost->getAll();

    //get all records from intro table
    $intro_topics = $db->getRows("SELECT * FROM intro ORDER BY intro_id");


    //get random quote from quotes table
    $quote = $db->get_single_row("SELECT * from quotes ORDER BY rand()");

    //include template files
    include('templates/header_template.php');
    include('templates/home_template.php');
    include('templates/footer_template.php');



 ?>
