
 <div class="container">

   <section id="contacts-content">
     <div class="row">
         <div class="col col12 left m-col12 s-col12">
           <!--section title-->
           <div class="section-title">
             <header>
               <h3><?php echo $contacts->content_title; ?></h3>
               <div class="title-stroke"></div>
             </header>
           </div>
           <p>
             <?php echo $contacts->content_text; ?>
           </p>

         </div>
     </div>
   </section>


 </div>
