<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="description" content="permaculture for the XXI century">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="permaculture, soil, compost">
    <link rel="stylesheet" type="text/css" href="static/css/main.css">
    <link rel="stylesheet" type="text/css" href="static/css/helper.css">
    <link rel="stylesheet" type="text/css" href="static/css/font-awesome4.7/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,700,700i" rel="stylesheet">
    <title>permaculture XXI</title>
  </head>

  <body>

  <!-- ******************* MAIN HEADER ********************** -->
      <header id="main-header">
          <!--logo-->
            <div class="logo">
              <h1><a href="index.php">P:XXI</a></h1>
            </div>

          <!--menu-->
          <div class="menu">
            <!--icon-->
            <i class="fa fa-bars"></i>
            <!--dropdown menu-->
            <div class="dropdown">
              <ul>
                <a href="about.php"><li>About</li></a>
                <a href="contacts.php"><li>Contacts</li></a>
                <a href="" class="search_text"><li>Search</li></a>
                <form action="search.php" method="GET" class="search_input">
                  <input type="text" name="search" placeholder="search" required/>
                </form>
              </ul>
            <!--end of dropdown-->
            </div>
          <!--end of menu-->
          </div>

      <!--end of main header-->
      </header>



  <!-- ****************** INTRO SECTION *********************** -->
      <section id="intro-section">
        <!--intro phrase-->
        <h3>don't change the world, change yourself</h3>
        <!--intro title-->
        <header class="page-title">
           <h2><span>perma</span><span>culture</span></h2>
           <p>care of the earth | care of people | fair share</p>
        </header>
      <!--end of intro-section-->
      </section>
