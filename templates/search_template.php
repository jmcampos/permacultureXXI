
    <div class="container">

      <section class="search-result">
        <p>Search results for: <b><?php echo $search; ?></b></p>
      </section>


      <div class="row" id="search">
          <?php foreach($search_result as $search) { ?>
          <div class="col col4 left m-col6 s-col12 post">
            <a href="<?php echo $search->post_link; ?>" target="_blank">
              <article class="example">
                <figure>
                  <img src="static/images/<?php echo $search->post_image; ?>">
                </figure>
                <header>
                  <h4>
                    <?php echo $search->post_title; ?>
                  </h4>
                  <p>
                    <?php echo $search->post_lead; ?>
                  </p>
                </header>
              </article>
            </a>
          </div>
          <?php } ?>
      </div>

    </div>
