

<!-- ****************** WHY  PERMACULTURE SECTION ************** -->
      <section id="why-section" class="section-content">
        <div class="container">

          <!--section title-->
          <div class="section-title">
            <header>
              <h3>why permaculture</h3>
              <div class="title-stroke"></div>
            </header>
          </div>

          <!--section article-->
          <article class="section-article">
            <!-- insert intro topics from database -->
            <?php foreach($intro_topics as $intro) { ?>
            <!--text-->
            <div class="article-text">
              <p>
                <?php echo $intro->intro_text; ?>
              </p>
            </div>
            <?php } ?>
          </article>

        <!--end of why section container-->
        </div>
      </section>


  <!-- *********************** QUOTES ***************** -->
      <section id="quote-section">
        <div class="container">
          <div class="quote">
            "<?php echo $quote->quote_text; ?>"<br>
            <span>- <?php echo $quote->quote_author; ?></span>
          </div>
        </div>
      <!--end of firt quote section-->
      </section>



  <!-- *********************  POSTS SECTION  ************ -->
      <section id="posts-section" class="section-content">
        <div class="container">

          <!--section title-->
          <div class="section-title">
            <header>
              <h3>resources</h3>
              <div class="title-stroke"></div>
            </header>
          </div>

          <!-- choose post categorie -->
          <div id="posts-categories" class="section-categorie">
            <p data-cat="all">All</p>
            <p data-cat="News">News</p>
            <p data-cat="Agriculture">Agriculture</p>
            <p data-cat="Blog">Blog</p>
          </div>

          <div class="row" id="posts">
              <?php foreach($posts as $post) { ?>
              <div class="col col4 left m-col6 s-col12 all post <?php echo $post->post_category; ?>">
                <a href="<?php echo $post->post_link; ?>" target="_blank">
                  <article class="example">
                    <figure>
                      <img src="static/images/<?php echo $post->post_image ?>" alt="">
                    </figure>
                    <header>
                      <h4><?php echo $post->post_title; ?></h4>
                      <p>
                        <?php echo $post->post_lead; ?>
                      </p>
                    </header>
                  </article>
                </a>
              </div>
              <?php } ?>

            <!--end of row-->
            </div>

          <!--end of container-->
          </div>
        <!--end of posts section-->
        </section>


      <!--back to top arrow-->
      <div class="top">
        <i class="fa fa-arrow-circle-up"></i><br>
        back to top
      </div>
