<?php

    include('includes/init.php');

    //new Post object
    $posts = new Post($db);

    //get list of all posts
    $listPosts = $posts->getAll();

    //get the post values
    if(isset($_POST['title'])) {
      $title = $db->escape($_POST['title']);
      $lead = $db->escape($_POST['lead']);
      $text = $db->escape($_POST['text']);
      $link = $db->escape($_POST['link']);
      $category = $_POST['category'];
      $image = '';
      if($_FILES['new-image']['error'] == 0) {
        $image = $_FILES['new-image']['name'];
        $tempname = $_FILES['new-image']['tmp_name'];
        move_uploaded_file($tempname, '../static/images/'.$image);
      }
      $posts->insertPost($title, $lead, $text, $link, $category, $image);
      header('location:posts.php');
      $posts->getAll();
    }

    //Change post status
    if(isset($_GET['status']) ){
      $post_id = $_GET['id'];
      $post_status = $_GET['status'];
      $posts->publishPost($post_id, $post_status);
      header('location:posts.php');
    }

    //Delete post
    if(isset($_GET['delete'])){
      $post_id = $_GET['id'];
      $post_delete = $_GET['delete'];
      $posts->deletePost($post_id, $post_delete);
      header('location:posts.php');
      $posts->getAll();
    }

    include('templates/header_template.php');
    include('templates/sidebar_template.php');
    include('templates/posts_template.php');
    include('templates/footer_template.php');



 ?>
