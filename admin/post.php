<?php


    include('includes/init.php');

    //new Post object
    $posts = new Post($db);

    //get the id of the psot
    if(isset($_GET['id'])) {
      $id = $_GET['id'];
      $post = $posts->getDetail($id);
    }

    //update post
    if(isset($_POST['save'])) {
      $title = $db->escape($_POST['title']);
      $lead = $db->escape($_POST['lead']);
      $text = $db->escape($_POST['text']);
      $link = $db->escape($_POST['link']);
      $category = $_POST['category'];
      $image = '';
      if($_FILES['new-image']['error'] == 0) {
        $image = $_FILES['new-image']['name'];
        $tempname = $_FILES['new-image']['tmp_name'];
        move_uploaded_file($tempname, '../static/images/'.$image);
      }
      $posts->editPost($title, $lead, $text, $link, $category, $image, $id);
      header("location:post.php?id='$id'");
      $posts->getDetail($id);
    }

    include('templates/header_template.php');
    include('templates/sidebar_template.php');
    include('templates/post_template.php');
    include('templates/footer_template.php');

 ?>
