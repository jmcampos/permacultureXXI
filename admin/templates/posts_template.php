
        <div class="col col10 m-col11 s-col11 left">
          <section class="main-container">

            <!--posts table-->
            <table>
              <tr class="table-title">
                <th class="medium">Date</th>
                <th class="big">Title</th>
                <th class="medium">Category</th>
                <th class="small">Published</th>
                <th class="small">Delete</th>
              </tr>
              <?php foreach($listPosts as $post) { ?>
              <tr class="table-text">
                <td><?php echo $post->post_date; ?></td>
                <td><a href="post.php?id=<?php echo $post->post_id;?>"><?php echo $post->post_title; ?></a></td>
                <td><?php echo $post->post_category; ?></td>
                <td>
                  <?php
                    if($post->post_publish == 1){
                      echo '<a href="posts.php?id='.$post->post_id.'&status=0"><i class="fa fa-check-circle green"></i></a>';
                    }else {
                      echo '<a href="posts.php?id='.$post->post_id.'&status=1"><i class="fa fa-ban red"></i></a>';
                    }
                  ?>
                </td>
                <td>
                  <?php echo '<a href="posts.php?id='.$post->post_id.'&delete=1"><i class="fa fa-trash"></i></a>'; ?>
                </td>
              </tr>
              <?php } ?>
            </table>

            <!--insert new post button-->
            <button id="new-post" type="button" name="button">New Post</button>

            <!--insert new post form-->
            <form class="new-form" action="" method="POST" enctype="multipart/form-data">
              <p class="title">Title</p>
              <textarea name="title" rows="1" cols="80"></textarea><br><br>
              <p class="title">Lead</p>
              <textarea name="lead" rows="4" cols="80"></textarea><br><br>
              <p class="title">Text</p>
              <textarea name="text" rows="8" cols="80"></textarea><br><br>
              <p class="title">Link</p>
              <textarea name="link" rows="2" cols="80"></textarea><br><br>
              <p class="title">Category</p>
              <select class="" name="category" value="<?php echo $_POST['category']; ?>">
                <option value="Blog">Blog</option>
                <option value="News">News</option>
                <option value="Agriculture">Agriculture</option>
              </select><br><br>
              <p class="title">Image</p>
              <input type="file" name="new-image" value=""><br><br>
              <input type="submit" name="" value="Guardar">
            </form>
          </section>
        </div>



      </div>
