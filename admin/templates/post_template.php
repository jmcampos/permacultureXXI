
        <div class="col col10 m-col11 s-col11 left">
          <section class="main-container">

              <form class="" action="" method="POST" enctype="multipart/form-data">
                <p class="title">Title</p>
                <textarea class="textarea" name="title" rows="1" cols="80" readonly><?php echo $post->post_title; ?></textarea><br><br>
                <p class="title">Lead</p>
                <textarea class="textarea" name="lead" rows="4" cols="80" readonly><?php echo $post->post_lead; ?></textarea><br><br>
                <p class="title">Text</p>
                <textarea class="textarea" name="text" rows="8" cols="80" readonly><?php echo $post->post_text; ?></textarea><br><br>
                <p class="title">Link</p>
                <textarea class="textarea" name="link" rows="2" cols="80" readonly><?php echo $post->post_link; ?></textarea><br><br>
                <p class="title">Category</p>
                <select class="select" name="category" value="" readonly>
                  <option value="Blog">Blog</option>
                  <option value="News">News</option>
                  <option value="Agriculture">Agriculture</option>
                </select><br><br>
                <p class="title">Image</p>
                <input class="input" type="file" name="new-image" value="" readonly><br><br>
                <input class="edit-button" type="button" name="edit" value="Edit">
                <input type="submit" name="save" value="Save">
              </form>
            </section>
          </div>
