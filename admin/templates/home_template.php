
      <div class="admin-title">
        <h3>Welcome "<?php echo $_SESSION['username'] ?>" to the administration page of Permaculture XXI</h3>
        <p>
          <!--timestamp-->
          <?php echo date("Y/m/d"); ?><br>
        </p>
      </div>

      <form id="logout-form" action="login.php" method="POST">
        <input type="submit" name="logout" value="Logout" />
      </form>
