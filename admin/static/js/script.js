//click event to display new post form
$("#new-post").click(function() {
  $(".new-form").toggle();
});

//click event on edit button
$(".edit-button").click(function() {
  $(".textarea").removeAttr("readonly");
  $(".select").removeAttr("readonly");
  $(".input").removeAttr("readonly");
});

//click event on save button
$(".save-button").click(function() {
  $(".textarea").attr("readonly", true);
  $(".select").attr("readonly", true);
  $(".input").attr("readonly", true);
});

//click event to display new intro form
$("#new-intro").click(function() {
  $(".new-form").toggle();
});
