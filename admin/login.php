<?php


    include('includes/init.php');

    //get list of users
    $user = $db->get_single_row("SELECT * FROM users");


    if(isset($_POST['username'])){
      $username = $db->escape($_POST['username']);
      $password = hash('sha256', $_POST['password']);

      //check if username and password match
      if($username == $user->username && $password == $user->password) {
        //Define username in Session array
        $_SESSION['username'] = $username;
        //redirect to admin home page
        header('location:home.php');
      } else {
        echo "Wrong username or password";
      }

    }


    if(isset($_POST['logout'])) {
      session_destroy();
    }



    include('templates/login_template.php');


 ?>
