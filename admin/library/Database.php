<?php

  class Database {

    private $cnx;
    private $hostname;
    private $user;
    private $password;
    private $database;


    //método construtor que recebe na altura da construcao os elementos necessários para a ligação
    public function __construct($hostname, $user, $password, $database) {
      $this->hostname = $hostname;
      $this->user = $user;
      $this->password = $password;
      $this->database = $database;

      //criar a ligação
      $this->cnx = new mysqli($this->hostname, $this->user, $this->password, $this->database);
      //verificar por erros na ligação
      if($this->cnx->connect_error) {
        die($this->cnx->connect_error);
      }
      //encode de caracteres
      $this->cnx->set_charset('utf8');
    }


    //método que verifica se há erros na última consulta à base de dados
    public function checkError() {
        if($this->cnx->error) {
          die($this->cnx->error);
        }
    }


    //método que efectua uma consulta à base de dados
    public function query($sql) {
      $this->cnx->query($sql);
      $this->checkError();
    }


    //método que efectua uma consulta, obtendo várias linhas
    public function getRows($sql) {
      $query = $this->cnx->query($sql);
      $this->checkError();
      $items = array();
      while($row = $query->fetch_object()) {
        $items[] = $row;
      }
      return $items;
    }


    //método que efectua uma consulta, obtendo apenas uma linha
    public function get_single_row($sql) {
      $query = $this->cnx->query($sql);
      $this->checkError();
      $item = $query->fetch_object();
      return $item;
    }

    //Escape string
    public function escape($string){
        return $this->cnx->real_escape_string($string);
    }


  }

 ?>
