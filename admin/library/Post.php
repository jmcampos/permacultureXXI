<?php


  class Post {
      //save database connection
      private $db;

      public function __construct($db) {
        $this->db = $db;
      }


      //method to list all posts
      public function getAll() {
        //query to search all elements from posts
        return $this->db->getRows("SELECT * FROM posts WHERE post_delete=0 ORDER BY post_date DESC");
      }

      public function getDetail($id) {
        //query to get details from course with given id
        return $this->db->get_single_row("SELECT * FROM posts WHERE post_id='$id'");
      }

      public function insertPost($title, $lead, $text, $link, $category, $image) {
        $this->db->query("INSERT INTO posts (
                                            post_title,
                                            post_lead,
                                            post_text,
                                            post_link,
                                            post_category,
                                            post_image
                                            )
                                      VALUES (
                                              '$title',
                                              '$lead',
                                              '$text',
                                              '$link',
                                              '$category',
                                              '$image'
                                              )
                          ");
      }

      public function deletePost($id, $delete) {
        $this->db->query("UPDATE posts SET post_delete='$delete' WHERE post_id='$id'");
      }

      public function publishPost($id, $status) {
        $this->db->query("UPDATE posts SET post_publish='$status' WHERE post_id='$id'");
      }


      public function editPost($title, $lead, $text, $link, $category, $image, $id) {

        $this->db->query("UPDATE posts SET
                                          post_title='$title',
                                          post_lead='$lead',
                                          post_text='$text',
                                          post_link='$link',
                                          post_category='$category',
                                          post_image='$image'
                                        WHERE post_id='$id'");
      }




  }



 ?>
